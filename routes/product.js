const express = require("express");
const connection = require("../connection");
const router = express.Router();

//for create

router.post("/create", (req, res, next) => {
  let product = req.body;
  query = "INSERT INTO product (name,description,price) VALUES ?";
  var values = [[product.name, product.description, product.price]];
  connection.query(query, [values], (err, results) => {
    if (!err) {
      return res.status(200).json({ message: "product Added Successfully" });
    } else {
      return res.status(500).json(err);
    }
  });
});

router.get("/getAllProduct", (req, res, next) => {
  var query = "select *from product";
  connection.query(query, (err, results) => {
    if (!err) {
      return res.status(200).json(results);
    } else {
      return res.status(500).json(err);
    }
  });
});

//for updation
router.patch("/update/:id", (req, res, next) => {
  console.log("request", req.body, req.params.id);
  const id = req.params.id;
  let product = req.body;
  var query = "UPDATE product SET name=?,description=?,price=? WHERE id=?";
  connection.query(
    query,
    [product.name, product.description, product.price, id],
    (err, results) => {
      if (!err) {
        if (results.affectedRows == 0) {
          return res.status(404).json({ message: "product id doesnot found" });
        }
        return res
          .status(200)
          .json({ message: "product updated successfully" });
      } else {
        return res.status(500).json(err);
      }
    }
  );
});

//for deletion
router.delete("/delete/:id", (req, res, next) => {
  const id = req.params.id;
  var query = "DELETE FROM product WHERE id=?";
  connection.query(query, [id], (err, results) => {
    if (!err) {
      if (results.affectedRows == 0) {
        return res.status(404).json({ message: "product id doesnot found" });
      }
      return res.status(200).json({ message: "product deleted successfully" });
    } else {
      return res.status(500).json(err);
    }
  });
});

module.exports = router;
