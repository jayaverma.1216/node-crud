const http = require("http");
const app = require("./index");

const host = "localhost";
const port = 3001; //to run

const server = http.createServer(app);

server.listen(port, host, () => {
  console.log(`Server is running on http://${host}:${port}`);
});
